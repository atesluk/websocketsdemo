package tab.edu.ws;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@ServerEndpoint(value = "/chat/")
public class ChatWebSocketEndpoint {

    private static List<ChatWebSocketEndpoint> webSocketEndpoints = new CopyOnWriteArrayList<ChatWebSocketEndpoint>();
    private Session session;

    @OnOpen
    public void onWebSocketConnect(Session sess) {
        System.out.println("Connected: " + sess);
        session = sess;
        webSocketEndpoints.add(this);
    }

    @OnMessage
    public void onWebSocketText(String message) throws IOException {
        System.out.println("Received TEXT message: " + message);
        broadcast(message);
    }

    @OnClose
    public void onWebSocketClose(CloseReason reason) {
        System.out.println("Socket Closed: " + reason);
        this.session=null;
    }

    @OnError
    public void onWebSocketError(Throwable cause) {
        cause.printStackTrace(System.err);
        session=null;
    }

    public void broadcast(String msg) throws IOException {
        for (ChatWebSocketEndpoint endpoint : webSocketEndpoints) {
            if(endpoint.session!=null){
            endpoint.session.getBasicRemote().sendText(msg);}
        }
    }

}
