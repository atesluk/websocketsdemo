package tab.edu.ws;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

public class Main {

	public static void main(String[] args) {
		Server server = new Server();
		ServerConnector connector = new ServerConnector(server);
		connector.setPort(8080);
		server.addConnector(connector);

		ServletContextHandler handler = new ServletContextHandler(
				ServletContextHandler.SESSIONS);
		handler.setContextPath("/");
		server.setHandler(handler);

		try {
			ServerContainer container = WebSocketServerContainerInitializer
					.configureContext(handler);
			container.addEndpoint(ChatWebSocketEndpoint.class);

			server.start();
			server.dump(System.err, "123");
			server.join();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

}
